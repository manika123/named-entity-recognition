""" A simple HTTP Server in Python
    for Named entity recognition.
    Author: Manika
    Created: 18-02-2018"""

#!flask/bin/python
import os
from flask import Flask, jsonify, abort, request, render_template, send_from_directory
from ner_baseline import NamedEntityRecognition
from ned_baseline import NamedEntityDisambiguation
from config import transitionProbabilityFile
from config import wordDistributionFile
from config import wikidataFile
from config import htmlFile
from config import separators

def multi_split(s, seprators):
    buf = [s]
    for sep in seprators:
        for loop, text in enumerate(buf):
            buf[loop:loop+1] = [i for i in text.split(sep) if i]
    return buf

app = Flask(__name__)

@app.route('/favicon.ico')
def favicon():
    return send_from_directory(os.path.join(app.root_path, 'static'),
                               'favicon.ico', mimetype='image/vnd.microsoft.icon')

@app.route('/<string:page_name>/')
def render_static(page_name):
    return render_template('%s.html' % page_name)

@app.route('/find_entity', methods=['POST'])
def find_entity():
    if not request.json or not 'text' in request.json:
        abort(400)

    ner = NamedEntityRecognition()

    # Read the probabilities from files for HMM
    print("Reading Transition Probabilities...")
    ner.read_transition_probs_from_file(transitionProbabilityFile)
    print("Reading Word Distribution...")
    ner.read_word_distribution_from_file(wordDistributionFile)

    # Get the text input from request 
    text = request.json['text']
    print(text)
    # Handle punctuations
    sentence = multi_split(text,separators)

    # Find named entities
    pos_tags = ner.pos_tag(sentence)
    named_entity = ner.find_named_entities(pos_tags)
    print("Entity found -->")
    print(named_entity)

    print("Looking up entity details...")
    # Iterate over the list of entities and diambiguate
    ned = NamedEntityDisambiguation()
    ned.read_wikidata_entities(wikidataFile)

    entity_details = []
    for name in named_entity:
        entity_details.append(ned.disambiguator(name))
    return jsonify({'success':1,'entity': entity_details}), 201

if __name__ == '__main__':
    app.run(debug=True)