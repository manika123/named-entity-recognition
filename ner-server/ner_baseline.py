""" A Simple Named Entity Recognition System 
    based on Hidden Markov Model using 
    Viterbi Algorithm.
    Author: Manika
    Create On: 15-02-2018"""

import config as c
import numpy as np

# Start word and tag
start_word = c.startWord
start_tag = c.startTag

# Stop word and tag
stop_word = c.stopWord
stop_tag = c.stopTag

# Named Entity tag
entity_tag = c.entityTag

# File paths
transition_probability_file = c.transitionProbabilityFile
word_distribution_file = c.wordDistributionFile

class NamedEntityRecognition:

    def __init__(self):
        self.transition_probabilities = None
        self.word_distribution = None
        self.known_words = None
        self.tags = None

    # Part-of-speech tagger
    def pos_tag(self,sentence):
        # add start and stop words
        words = [start_word] + sentence + [stop_word]
        
        # score and path matrices
        rows = len(words)
        cols = len(self.tags)
        score = np.zeros((rows,cols))
        path = np.full((rows,cols),-1)
        
        for word in words:
            if word not in self.known_words:
                self.known_words.add(word)
        num_words = len(self.known_words)
        
        # Forward Step
        for i, word in enumerate(words):
            if i==0:
                score[i][self.tags.index(start_tag)]=1
                continue
            for j, next_tag in enumerate(self.tags):
                emit_p = self.get_emission_prob(next_tag,word)
                # Smoothing
                emit_p = (1-0.00001)*emit_p + 0.00001/num_words
                for k, prev in enumerate(self.tags):
                    trans_p = self.get_transition_prob(prev,next_tag)
                    prob = score[i-1][k]*trans_p *emit_p
                    if score[i][j]<prob:
                        score[i][j]=prob
                        path[i][j]=k
        # Backward Step
        tagging = []
        idx = self.tags.index(stop_tag)
        for l in range(rows-1,-1,-1):
            tagging.append((words[l],self.tags[idx]))
            idx = int(path[l][idx])
        tagging = list(reversed(tagging[1:-1]))
        
        return tagging

    # Named entity finder
    def find_named_entities(self,tagging):
        entities = []
        prev_tag = [0 for i in range(len(tagging))]
        for i in range(len(tagging)):
            if tagging[i][1]==entity_tag :
                if prev_tag[i-1] == 1:
                    entities[len(entities)-1]=entities[len(entities)-1]+" "+tagging[i][0]
                else:
                    entities.append(tagging[i][0])
                prev_tag[i]=1
        return entities
    
    # Read Transition Probabilities
    def read_transition_probs_from_file(self, filename):
        transition_probabilities = {}
        with open(filename,encoding='utf8') as f:
            for line in f:
                tag1, tag2, prob = line.strip().split("\t")

                if tag1 not in transition_probabilities:
                    transition_probabilities[tag1] = {}
                transition_probabilities[tag1][tag2] = float(prob)

        self.transition_probabilities = transition_probabilities

    # Read Word Distribution
    def read_word_distribution_from_file(self, filename):
        word_distribution = {}
        tags = set()
        known_words = set()

        with open(filename,encoding='utf8') as f:
            word_distribution[start_tag] = {start_word: 1}
            tags.add(start_tag)
            known_words.add(start_word)
            for line in f:
                word, tag, prob = line.strip().split("\t")

                if tag not in word_distribution:
                    word_distribution[tag] = {}
                word_distribution[tag][word] = float(prob)
                tags.add(tag)
                known_words.add(word)

            word_distribution[stop_tag] = {stop_word: 1}
            tags.add(stop_tag)
            known_words.add(stop_word)

        self.word_distribution = word_distribution
        self.known_words = known_words
        self.tags = list(tags)

    # Return transition probability for given tags
    def get_transition_prob(self, tag1, tag2):
        return self.transition_probabilities.get(tag1, {}).get(tag2, 0)

    # Return emission probability for given tag and word
    def get_emission_prob(self, tag, word):
        return self.word_distribution.get(tag, {}).get(word, 0)

if __name__ == "__main__":
    ner = NamedEntityRecognition()
    ner.read_word_distribution_from_file(word_distribution_file)
    ner.read_transition_probs_from_file(transition_probability_file)
    #print(ner.pos_tag(["James", "Bond", "is", "an", "agent"]))
    print(ner.find_named_entities(ner.pos_tag(["James", "Bond", "is", "an", "agent"])))