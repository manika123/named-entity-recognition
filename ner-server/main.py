""" Main file for Named Entity Recognition and
    Named Entity Disambiguation.
    Calls the classes from ***_baseline.py files.
    Author: Manika
    Created On: 18-02-2018"""


USAGE = """
USAGE: py main.py <transition probabilities file> <word distribution file> <sentence>
Example>>>
py main.py
OR
py main.py transition-probabilities.tsv word-distribution.tsv "James Bond is an agent"
OR
py main.py transition-probabilities.tsv word-distribution.tsv
"""

import sys
import config as c
from ner_baseline import NamedEntityRecognition
from ned_baseline import NamedEntityDisambiguation

# Symbols
separators = c.separators

def multi_split(s, seprators):
    buf = [s]
    for sep in seprators:
        for loop, text in enumerate(buf):
            buf[loop:loop+1] = [i for i in text.split(sep) if i]
    return buf

if __name__ == "__main__":
    argLen = len(sys.argv)
    if argLen>4 or argLen==2:
        print(USAGE)
        quit()

    # Get filenames
    if argLen==3:
        trans_prob_file = sys.argv[1]
        word_dis_file = sys.argv[2]
    else:
        trans_prob_file = c.transitionProbabilityFile
        word_dis_file = c.wordDistributionFile
	
	# Use the input provided or get input sentence
    if argLen == 4:
        sentence = multi_split(sys.argv[3],separators)
    else: 
        text = input("\nEnter the text:")
        # Handle punctuations
        sentence = multi_split(text,separators)

    # Find the Named Entity in the input array of strings
    ner = NamedEntityRecognition()

    # Read the probabilities from files for HMM
    print("Reading Transition Probabilities...")
    ner.read_transition_probs_from_file(trans_prob_file)
    print("Reading Word Distribution...")
    ner.read_word_distribution_from_file(word_dis_file)

    print("Finding entity/entities...")
    # Part of speech tagging for entity recognition
    pos_tags = ner.pos_tag(sentence)
    named_entity = ner.find_named_entities(pos_tags)
    print("Entity found -->")
    print(named_entity)

    print("Looking up entity details...")
    # Iterate over the list of entities and diambiguate
    ned = NamedEntityDisambiguation()
    ned.read_wikidata_entities(c.wikidataFile)

    entity_details = {}
    for name in named_entity:
        entity_details[name] = ned.disambiguator(name)

    print(entity_details)
