""" A Simple Named Entity Disambiguator
    based on popularity score 
    from approx. 3 million entries from Wikidata.
    Author: Manika
    Created on: 17-02-2018"""

from config import wikidataFile

class NamedEntityDisambiguation:
    
    # initialise the class
    def __init__(self):
        # entries from the file 
        self.entity = None
    
    def get_entity_score(self,name):
        return self.entity.get(name,{}).get("score",0)
    
    def get_entity(self,name):
        return self.entity.get(name,{})
    
    # Read Wikidata Entities
    def read_wikidata_entities(self, filename):
        wiki_data = {}
        # Read from file
        with open(filename,encoding='utf8') as f:
            # Add the details from the file to the class.
            
            for i,line in enumerate(f):
                temp = {}
                #synonyms = []
                if i == 0:
                    continue
                info = line.strip().split("\t")
                temp["Name"] = info[0]
                temp["score"] = int(info[1])
                temp["description"] = info[2]
                temp["url"] = info[3]
                temp["id"] = info[4]
                
                # Handle the list of synonyms for the entries that have synonyms
                try:
                    synonyms = info[5]
                    #synonyms = info[5].split(";")
                except IndexError:
                    synonyms = ""
                    #synonyms = []
                temp["synonyms"] = synonyms
                
                if info[0] not in wiki_data:
                    wiki_data[info[0]]=temp
                
            self.entity = wiki_data
        
    def disambiguator(self,name):
        
        # Return the entity with highest score from wiki_data
        # Add the ability to handle synonyms later
        s = self.get_entity_score(name)
        if s!=0:
            return self.get_entity(name)
        else:
            return 0

if __name__ == "__main__":
    ned = NamedEntityDisambiguation()
    ned.read_wikidata_entities(wikidataFile)
    print(ned.disambiguator("Russia"))