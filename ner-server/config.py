""" Config file for server.py 
    Author: Manika
    Created On: 19-02-2018"""

# TSV File paths
transitionProbabilityFile = "files/transition-probabilities.tsv"
wordDistributionFile = "files/word-distribution.tsv"
wikidataFile = "files/wikidata-entities.tsv"
NerDatasetFile = "files/ner_dataset.csv"

# HTML File paths
htmlFile = "www/html/entity.html"

# Start word and tag
startWord = "^"
startTag = "BEG"

# Stop word and tag
stopWord = "$"
stopTag = "END"

# Named Entity tag
entityTag = "NNP"

# Symbols
separators = ' ,.?!";:'